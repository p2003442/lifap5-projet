/* ******************************************************************
 * Constantes de configuration
 * ****************************************************************** */
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami(apiKey) {
  return fetch(serverUrl + "/whoami", { headers: { "Api-Key": apiKey } })
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête GET sur /pokemon
 * @returns une promesse de tableau pokemon ou du message d'erreur
 */
function fetchPokemon() {
  return fetch(serverUrl + "/pokemon")
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête GET sur /deck/:id
 * @returns une promesse de tableau du deck
 */
function fetchDeck() {
  return fetch(serverUrl + "/deck"+etatCourant.login)
    .then((response) => {
      if (response.status === 401) {
        return response.json().then((json) => {
          console.log(json);
          return { err: json.message };
        });
      } else {
        return response.json();
      }
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans la modale d'affichage
 * de l'utilisateur puis déclenche l'affichage de cette modale.
 *
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(etatCourant, apiKey) {
  return fetchWhoami(apiKey).then((data) => {
    majEtatEtPage(etatCourant, {
      login: data.user, // qui vaut undefined en cas d'erreur
      errLogin: data.err, // qui vaut undefined si tout va bien
      loginModal: true, // on affiche la modale
    });
  });
}

/**
 * Génère le code HTML du corps de la modale de login. On renvoie en plus un
 * objet callbacks vide pour faire comme les autres fonctions de génération,
 * mais ce n'est pas obligatoire ici.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et un objet vide
 * dans le champ callbacks
 */
function genereModaleLoginBody(etatCourant) {
  const text =
    etatCourant.errLogin !== undefined
      ? etatCourant.errLogin
      : etatCourant.login;
  return {
    html: `
  <section class="modal-card-body">
    <p>b80bdf85-47d9-405d-856b-a933d93d5c00</p>
    <input type="password" id="apiKey"  placeholder="ecrit ta clef de connexion">
    <p>${text}</p>
  </section>
  `,
    callbacks: {},
  };
}

/**
 * Génère le code HTML du titre de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginHeader(etatCourant) {
  return {
    html: `
<header class="modal-card-head  is-back">
  <p class="modal-card-title">Utilisateur</p>
  <button
    id="btn-close-login-modal1"
    class="delete"
    aria-label="close"
    ></button>
</header>`,
    callbacks: {
      "btn-close-login-modal1": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
    },
  };
}

/**
 * Génère le code HTML du base de page de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLoginFooter(etatCourant) {
  return {
    html: `
  <footer class="modal-card-foot" style="justify-content: flex-end">
    <button id="btn-close-login-modal2" class="button">Fermer</button>
    <button id="btn-valide-login-modal2" class="button">Valider</button>
  </footer>
  `,
    callbacks: {
      "btn-close-login-modal2": {
        onclick: () => majEtatEtPage(etatCourant, { loginModal: false }),
      },
      "btn-valide-login-modal2": {
        onclick: () => lanceWhoamiEtInsereLogin( etatCourant,document.getElementById("apiKey").value ),
      },
    },
  };
}

/**
 * Génère le code HTML de la modale de login et les callbacks associés.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereModaleLogin(etatCourant) {
  const header = genereModaleLoginHeader(etatCourant);
  const footer = genereModaleLoginFooter(etatCourant);
  const body = genereModaleLoginBody(etatCourant);
  const activeClass = etatCourant.loginModal ? "is-active" : "is-inactive";
  return {
    html: `
      <div id="mdl-login" class="modal ${activeClass}">
        <div class="modal-background"></div>
        <div class="modal-card">
          ${header.html}
          ${body.html}
          ${footer.html}
        </div>
      </div>`,
    callbacks: { ...header.callbacks, ...footer.callbacks, ...body.callbacks },
  };
}

/* ************************************************************************
 * Gestion de barre de navigation contenant en particulier les bouton Pokedex,
 * Combat et Connexion.
 * ****************************************************************** */

/**
 * Déclenche la mise à jour de la page en changeant l'état courant pour que la
 * modale de login soit affichée
 * @param {Etat} etatCourant
 */
function afficheModaleConnexion(etatCourant) {
  lanceWhoamiEtInsereLogin(etatCourant);
}

/**
 * Génère le code HTML et les callbacks pour la partie droite de la barre de
 * navigation qui contient le bouton de login.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonConnexion(etatCourant) {
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <div class="buttons">
        <a id="btn-open-login-modal" class="button is-light"> Connexion </a>
      </div>
    </div>
  </div>`;
  return {
    html: html,
    callbacks: {
      "btn-open-login-modal": {
        onclick: () => afficheModaleConnexion(etatCourant),
      },
    },
  };
}

/**
 * Génère le code HTML et les callbacks pour la partie droite de la barre de
 * navigation qui contient le bouton de déconnexion.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBoutonDeconnexion(etatCourant) {
  const html = `
  <div class="navbar-end">
    <div class="navbar-item">
      <p>${etatCourant.login}</p>
      <div class="buttons">
        <a id="btn-open-login-modal" class="button is-light"> Déconnexion </a>
      </div>
    </div>
  </div>`;
  return {
    html: html,
    callbacks: {
      "btn-open-login-modal": {
        onclick: () => lanceWhoamiEtInsereLogin(etatCourant,null),
      },
    },
  };
}

/**
 * Génère le code HTML de la barre de navigation et les callbacks associés.
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function genereBarreNavigation(etatCourant) {
  const connexion = (etatCourant.login === undefined)?genereBoutonConnexion(etatCourant)
                                                      :genereBoutonDeconnexion(etatCourant);
  return {
    html: `
  <nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar">
      <div class="navbar-item"><div class="buttons">
          <a id="btn-pokedex" class="button is-light"> Pokedex </a>
          <a id="btn-combat" class="button is-light"> Combat </a>
      </div></div>
      ${connexion.html}
    </div>
  </nav>`,
    callbacks: {
      ...connexion.callbacks,
      "btn-pokedex": { onclick: () => console.log("click bouton pokedex") },
    },
  };
}

/**
 * récupère le tableau des pokemons
 * @param {etat} etatCourant
 * @returns
 */
 function recupTableauPokemon(etatCourant) {
  return fetchPokemon().then(json=>majEtatEtPage(etatCourant,{pokemon:json}));
}

/**
 * récupère le tableau du deck
 * @param {etat} etatCourant 
 * @returns retourne une promesse
 */
function recupTableauDeck(etatCourant) {
  return fetchPokemon().then(json=>majEtatEtPage(etatCourant,{deck:json}));
}

/**
 * convertis un tableau en affichage html avec retour à la ligne
 * @param {*} tableau 
 * @returns 
 */
function tableauToHtml(tableau) {
  return `<ul>${tableau.reduce((acc,mot)=>acc +'<li>'+ mot+'</li>')} <ul>`;
}

/**
 * Code html de L'en Tête du Tableau
 * @param {array} tabl 
 * @returns 
 */
function enTeteTableau(etatCourant,tabl){
  return{html:enTetePokedex(etatCourant).html+`<div id="tbl-pokemons">
        <table class="table">
          <thead>
            <tr>
              <th><span>Image</span></th>
              <th>
                <span>#</span
                ><span class="icon"><i class="fas fa-angle-up"></i></span>
              </th>
              <th><span>Noms</span></th>
              <th><span>Abilitées</span></th>
              <th><span>Types</span></th>
            </tr>
          </thead>
        <tbody>`+ tabl +`</tbody>
        </table>
      </div>
  </div>`
  }
}

/**
 * 
 * @returns le html pour l'entête du pokedex
 */
function enTetePokedex(etatCourant){
  return { html:
  `<section class="section">
      <div class="columns">
        <div class="column">
          <div class="tabs is-centered">
            <ul>
              <li class="is-active" id="tab-all-pokemons">
                <a>Tous les pokemons</a>
              </li>
              <li id="tab-tout"><a>Mes pokemons</a></li>
            </ul>
          </div>
  `,
  callback:{
    "tab-tout":{onClick: () => recupTableauDeck(etatCourant),},
  },
}
}

/**
 * 
 * @param {object} pokeSelected 
 * @returns html du header du détail
 */
function genereDetailHeader(pokeSelected){
  return `
  <div class="column">
    <div class="card">
      <div class="card-header">
        <div class="card-header-title">${pokeSelected.JapaneseName} (#${pokeSelected.PokedexNumber})</div>
      </div>`
}

/**
 * 
 * @returns html du footer détail 
 */
function genereDetailFooter(){
  return `<div class="card-footer">
      <article class="media">
        <div class="media-content">
          <button class="is-success button" tabindex="0">
            Ajouter à mon deck
          </button>
        </div>
      </article>
    </div>`
}

/**
 * 
 * @param {Etat} etatCourant 
 * @returns le code html pour générer le Detail du pokemon selectionner
 */
function genereDetailPokemon(etatCourant){
  const pokeSelected=etatCourant.pokemon.find(poke => poke.PokedexNumber===etatCourant.detailPokemon);
  return {html:`${genereDetailHeader(pokeSelected)}
    <div class="card-content">
      <article class="media">
        <div class="media-content">
          <h1 class="title">${pokeSelected.Name}</h1>
        </div>
      </article>
    </div>
    <div class="card-content">
      <article class="media">
        <div class="media-content">
          <div class="content has-text-left">
            <p>point de Dégats: ${pokeSelected.Attack}</p>
            <h3>Abilitées</h3>
            ${tableauToHtml(pokeSelected.Abilities)}
            <h3>Resistant contre</h3>
            ${tableauToHtml(Object.entries(pokeSelected.Against).filter( ([nom,indice]) => indice < 1 ))}
            <h3>Faiblesse contre</h3>
            ${tableauToHtml(Object.entries(pokeSelected.Against).filter( ([nom,indice]) => indice > 1 ))}
          </div>
        </div>
        <figure class="media-right">
          <figure class="image is-475x475">
            <img
              class=""
              src=${pokeSelected.Images.Full}
              alt=${pokeSelected.Name}
            />
          </figure>
        </figure>
      </article>
    </div>
    ${genereDetailFooter()}
  </div> </div> </section>`,
  callbacks:{},};
}

/**
 * augmente la limite d'affichage des pokemons de 10
 * @param {Etat} etatCourant 
 */
function augmenteLimitePoke(etatCourant){
  if( etatCourant.limitePoke+10 < etatCourant.pokemon.length )
    majEtatEtPage(etatCourant,{limitePoke: etatCourant.limitePoke+10});
  else console.log("tout les pokemons sont afficher");
}

/**
 * diminue la limite d'affichage des pokemons de 10
 * @param {Etat} etatCourant 
 */
function diminueLimitePoke(etatCourant){
  if( etatCourant.limitePoke > 10 )
    majEtatEtPage(etatCourant,{limitePoke: etatCourant.limitePoke-10});
  else console.log("limite de 10 pokemon");
}

/**
 * 
 * @param {Etat} etatCourant 
 * @returns l'html des boutons plus et moins du tableau et les callbacks en cas de clics
 */
function genereBoutonPlusMoins(etatCourant){
  const htmlBoutons = `<div class="buttons">
    <button id="btn-Plus" class="button">Plus</button> 
    <button id="btn-Moins" class="button">Moins</button>
  </div>`;
  return {
    html: htmlBoutons,
    callbacks: {
      "btn-Plus": {
        onclick: () => augmenteLimitePoke(etatCourant) ,
      },
      "btn-Moins": {
        onclick: () => diminueLimitePoke(etatCourant) ,
      },
    },
  }
}

/**
 * permet de faire le callback pour savoir quelle pokemon est selectionné
 * @param {ETAT} etatCourant 
 * @param {POKEMON} mot 
 * @returns 
 */
function callbackClickSurLePoke(etatCourant,mot){
  const callback={};
  callback[`poke num:${mot.PokedexNumber}`]={
    onclick:() => majEtatEtPage(etatCourant, {detailPokemon: mot.PokedexNumber})
  };
  return callback;
}

/**
 * genere le tableau des pokemons
 * @param {etat} etatCourant
 */
function genereTableauPokemon(etatCourant){
  const tabTrie = etatCourant.pokemon.sort((a, b) => a.PokedexNumber - b.PokedexNumber );
  const tabLimite = tabTrie.filter((pokemon,indice) => indice < etatCourant.limitePoke);
  const tabl = tabLimite.reduce((acc,mot)=>acc+`
  <tr id="poke num:${mot.PokedexNumber}" class="${mot.PokedexNumber === etatCourant.detailPokemon?`is-selected`:``}">
    <td>
      <img alt=${mot.Name}
        src=${mot.Images.Detail}
        width="64" />
    </td>
    <td><div class="content">${mot.PokedexNumber}</div></td>
    <td><div class="content">${mot.Name}</div></td>
    <td>${tableauToHtml(mot.Abilities)}</td>
    <td>${tableauToHtml(mot.Types)}</td>
  </tr>
  `,``);
  const tablCall = tabLimite.map( (callNum)=>callbackClickSurLePoke(etatCourant,callNum))
  return { html: enTeteTableau(etatCourant,tabl).html,
    callbacks: tablCall.reduce((acc,call)=>({...acc,...call}),{}),
  };
}

/**
 * Génére le code HTML de la page ainsi que l'ensemble des callbacks à
 * enregistrer sur les éléments de cette page.
 *
 * @param {Etat} etatCourant
 * @returns un objet contenant le code HTML dans le champ html et la description
 * des callbacks à enregistrer dans le champ callbacks
 */
function generePage(etatCourant) {
  const barredeNavigation = genereBarreNavigation(etatCourant);
  const modaleLogin = genereModaleLogin(etatCourant);
  const tableauPokemon = genereTableauPokemon(etatCourant);
  const boutonPlusMoins = genereBoutonPlusMoins(etatCourant);
  const DetailPokemon = genereDetailPokemon(etatCourant);
  // remarquer l'usage de la notation ... ci-dessous qui permet de "fusionner"
  // les dictionnaires de callbacks qui viennent de la barre et de la modale.
  // Attention, les callbacks définis dans modaleLogin.callbacks vont écraser
  // ceux définis sur les mêmes éléments dans barredeNavigation.callbacks. En
  // pratique ce cas ne doit pas se produire car barreDeNavigation et
  // modaleLogin portent sur des zone différentes de la page et n'ont pas
  // d'éléments en commun.
  return {
    html: barredeNavigation.html + modaleLogin.html + tableauPokemon.html + DetailPokemon.html + boutonPlusMoins.html,
    callbacks: { ...barredeNavigation.callbacks, ...modaleLogin.callbacks,  ...tableauPokemon.callbacks, ...boutonPlusMoins.callbacks, ...DetailPokemon.callbacks, ...enTetePokedex.callback},
  };
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Créée un nouvel état basé sur les champs de l'ancien état, mais en prenant en
 * compte les nouvelles valeurs indiquées dans champsMisAJour, puis déclenche la
 * mise à jour de la page et des événements avec le nouvel état.
 *
 * @param {Etat} etatCourant etat avant la mise à jour
 * @param {*} champsMisAJour objet contenant les champs à mettre à jour, ainsi
 * que leur (nouvelle) valeur.
 */
function majEtatEtPage(etatCourant, champsMisAJour) {
  const nouvelEtat = { ...etatCourant, ...champsMisAJour };
  majPage(nouvelEtat);
}

/**
 * Prend une structure décrivant les callbacks à enregistrer et effectue les
 * affectation sur les bon champs "on...". Par exemple si callbacks contient la
 * structure suivante où f1, f2 et f3 sont des callbacks:
 *
 * { "btn-pokedex": { "onclick": f1 },
 *   "input-search": { "onchange": f2,
 *                     "oninput": f3 }
 * }
 *
 * alors cette fonction rangera f1 dans le champ "onclick" de l'élément dont
 * l'id est "btn-pokedex", rangera f2 dans le champ "onchange" de l'élément dont
 * l'id est "input-search" et rangera f3 dans le champ "oninput" de ce même
 * élément. Cela aura, entre autres, pour effet de délclencher un appel à f1
 * lorsque l'on cliquera sur le bouton "btn-pokedex".
 *
 * @param {Object} callbacks dictionnaire associant les id d'éléments à un
 * dictionnaire qui associe des champs "on..." aux callbacks désirés.
 */
function enregistreCallbacks(callbacks) {
  Object.keys(callbacks).forEach((id) => {
    const elt = document.getElementById(id);
    if (elt === undefined || elt === null) {
      console.log(
        `Élément inconnu: ${id}, impossible d'enregistrer de callback sur cet id`
      );
    } else {
      Object.keys(callbacks[id]).forEach((onAction) => {
        elt[onAction] = callbacks[id][onAction];
      });
    }
  });
}

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  const page = generePage(etatCourant);
  document.getElementById("root").innerHTML = page.html;
  enregistreCallbacks(page.callbacks);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientPokemons() {
  console.log("CALL initClientPokemons");
  const etatInitial = {
    loginModal: false,
    login: undefined,
    errLogin: undefined,
    limitePoke: 10,
    detailPokemon:1,
  };
  //majPage(etatInitial);
  recupTableauPokemon(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientPokemons();
});
